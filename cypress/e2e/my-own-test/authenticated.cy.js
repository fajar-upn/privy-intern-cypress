/// <reference types="cypress" />

// token
const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImZhamFyIiwiX2lkIjoiNjMyMTU1MmRjNGQwZTMwMDA5MTY4ZmMwIiwibmFtZSI6Ik11aGFtbWFkIEZhamFyIiwiaWF0IjoxNjYzMTI5NDQ3LCJleHAiOjE2NjgzMTM0NDd9.zD72PZG_huJBxLdkqEPqCwbvDY27DgsI_nR7MEX40PU"

describe('Basic Authenticated desktop Test', () => {
    before(() => {
        window.localStorage.setItem('__auth__token', token)
    })

    beforeEach(() => {
        cy.viewport(1280, 720);
        cy.visit("https://codedamn.com");
    })

    it('should pass', () => {
        cy.visit('https://codedamn.com/playground/PCFbO2GWjVPbmxmdGGxop')
        cy.log('Checking bottom connections')

        cy.contains('Check bottom left left button').should('exist')
        cy.get('[data-testid=xterm-controls] > div').should('contain.text', 'connecting')

        cy.get("Trying to establish connecting").should('exist')

        cy.log('Playground is initializing')
        cy.get("Setting up the challenge").should('exist')
        cy.get("Setting up the challenge", { timeout: 1 * 1000 }).should('not.exist') //timeout is auto failed


        // cy.get('div')
        // cy.pause()
        // cy.debug()
    });

    it('should pass', () => {
        cy.visit('https://codedamn.com/playground/PCFbO2GWjVPbmxmdGGxop')

        // cy.get("Setting up the challenge").should('exist')
        // cy.get("Setting up the challenge", { timeout: 1 * 1000 }).should('not.exist') //timeout is auto failed

        cy.get(['class=xterm-helper-textarea'])
            .type('{ctrl}{c}')
            .type("touch testscript.js{enter}")

        cy.contains('testscript.js').should('exist')

    });

    it('Create new file', () => {
        cy.visit('https://codedamn.com/playground/PCFbO2GWjVPbmxmdGGxop')

        // cy.get("Setting up the challenge").should('exist')
        // cy.get("Setting up the challenge", { timeout: 1 * 1000 }).should('not.exist') //timeout is auto failed

        cy.get(['class=xterm-helper-textarea'])
            .type('{ctrl}{c}')
            .type("touch testscript.js{enter}")

        cy.contains('testscript.js').should('exist')

    });

    it('right click pass', () => {
        cy.visit('https://codedamn.com/playground/PCFbO2GWjVPbmxmdGGxop')

        // cy.get("Setting up the challenge").should('exist')
        // cy.get("Setting up the challenge", { timeout: 1 * 1000 }).should('not.exist') //timeout is auto failed

        cy.get(['class=xterm-helper-textarea'])
            .type('{ctrl}{c}')
            .type("touch testscript.js{enter}")

        cy.contains('testscript.js').rightclick()

        cy.contains('Rename File').click()

        cy.get('[data-testid=renamefilefolder]').type('new_testscripts.js')

        cy.get('[data-testid=renamebtn]').click()

        cy.contains('new_testscripts').should('exist')
    });


});
