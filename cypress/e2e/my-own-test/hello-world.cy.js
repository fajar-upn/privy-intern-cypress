/// <reference types="cypress" />

describe('Basic desktop Test', () => {

    /**
    * Before and Before Each sections
    * this code for auto load
    */
    beforeEach(() => {
        cy.viewport(1280, 720);
        cy.visit('https://codedamn.com');
    });

    it('We have correct page title', () => {

        // view port : for organize size (width and height)
        // cy.viewport('iphone-4')
        // cy.viewport(1280, 720)

        // visit : for visit certain website appropriate URL
        cy.visit('https://codedamn.com')

        /**
         * contains() or get() section
         */
        // way 1
        // cy.contains(`Explore All Learning Paths`)

        // way 2
        // cy.get('div > .leading-6')

        // way 3
        // cy.get('[data-testid="homepage-cta"]')

        // cy.get('div#root').should('exist')
        // cy.get('div[id=root]').should('exist')

        /**
         * contains.should() section
         */
        // click testing by e2e test
        // cy.get('[data-testid="homepage-cta"]').click()

        // this code should contains text with 'Learn Programming'
        // cy.contains('Learn Programming').should('have.text', 'Learn Programming')


        /**
         * click() function
         */
        // cy.contains('Explore All Roadmaps').click()
    })

    /**
     * Login n2n Test
     */
    // it.only() : function for run this function only
    it('Login pages work properly', () => {
        // cy.viewport(1280, 720)
        cy.contains('Sign in').click()
        cy.contains('Sign in to codedamn').should('exist')
        cy.contains('Sign in with Google').should('exist')
    })

    /**
     * Login n2n test links
     */
    it('Create Account URL test', () => {
        cy.viewport(1280, 720)

        /**
         * for test links we must filled this step:
         * 1. go to sign in page
         * 2. log URL
         * 3. click creat account URL
         * 4. verify URL page
         * 5. back to login page
         */
        cy.contains('Sign in').click()

        cy.url().then(value => {
            cy.log('current URL', value)
        });
        // note if use console log will appear in website

        cy.contains('Create one').click()

        cy.url().should('include', '/register')
        cy.go('back')
    });

    /**
     * Login Test
     */
    it('Login test with input id and password', () => {
        cy.viewport(1280, 720)
        cy.visit('https://codedamn.com')
        cy.contains('Sign in').click()
        cy.get('[data-testid="username"]').type('admin', { force: true }) //.type('') is for auto type
        cy.get('[data-testid="password"]').type('admin', { force: true }) //.type('') is for auto type

        cy.get('[data-testid="login"]')

        cy.contains('Unable to authorize.').should('exist')
    });

    it.only('Login test should work fine', () => {

        // set token for auto login
        // const token = 

        // cy.viewport(1280, 720)
        cy.contains('Sign in').click()
        cy.get('[data-testid="username"]').type('muhammadfajar1183@gmail.com', { force: true }) //.type('') is for auto type
        cy.get('[data-testid="username"]').type('muhammadfajar1183@gmail.com', { force: true }) //.type('') is for auto type
        cy.get('[id="password"]').type('Fajarsidiq99', { force: true }) //.type('') is for auto type

        cy.get('[data-testid="login"]').click({ force: true })

        cy.url().should('include', '/dashboard')
    });



});